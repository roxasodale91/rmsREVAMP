﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rmsREVAMP.Models
{
    //Extended Identity Roles
    public class ApplicationRoles : IdentityRole
    {
        public override ICollection<IdentityUserRole> Users
        {
            get
            {
                return base.Users;
            }
        }
        public bool isViewOnlyMaterial { get; set; }
        public bool canCreateMaterial { get; set; }
        public bool canModifyMaterial { get; set; }
        public bool isViewOnlyRecipe { get; set; }
        public bool canCreateRecipe { get; set; }
        public bool canModifyRecipe { get; set; }
        public string Description { get; set; }
        public string Creator { get; set; }
        public string DateCreated { get; set; }
        public string Modifier { get; set; }
        public string DateModified { get; set; }
        public bool isActive { get; set; }


        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}