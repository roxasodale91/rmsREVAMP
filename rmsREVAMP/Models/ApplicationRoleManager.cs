﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Data.Entity;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Owin;
using Microsoft.AspNet.Identity.Owin;

namespace rmsREVAMP.Models
{
    public class ApplicationRoleManager : RoleManager<ApplicationRoles>
    {
        public ApplicationRoleManager(
            IRoleStore<ApplicationRoles, string> roleStore)
            : base(roleStore)
        {
        }
        public static ApplicationRoleManager Create(
            Microsoft.AspNet.Identity.Owin.IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            return new ApplicationRoleManager(
                new RoleStore<ApplicationRoles>(context.Get<ApplicationDbContext>()));
        }
    }
}