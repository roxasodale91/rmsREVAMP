﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace rmsREVAMP.Models
{
    public class RoleManagementViewModels
    {
        public ApplicationUser applicationUser = new ApplicationUser();
        public ApplicationRoles applicationRoles = new ApplicationRoles();

        public List<ApplicationUser> appicationUserList = new List<ApplicationUser>();
        public List<ApplicationRoles> applicationRolesList = new List<ApplicationRoles>();

        public IdentityRole identityRole = new IdentityRole();
        public IdentityUser identityUser = new IdentityUser();

        public List<IdentityRole> identityRoleList = new List<IdentityRole>();
        public List<IdentityUser> identityUserList = new List<IdentityUser>();

        public List<Department> departmentList = new List<Department>();
        public Department department = new Department();

        public List<UserDepartment> userDepartmentList = new List<UserDepartment>();
        public UserDepartment userDepartment = new UserDepartment();

        public UserRoleRight UserRoleRight = new UserRoleRight();
        public List<UserRoleRight> UserRoleRightList = new List<UserRoleRight>();

    }
}