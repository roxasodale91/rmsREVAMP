﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Data.Entity;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations.Schema;

namespace rmsREVAMP.Models
{

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("User");
            modelBuilder.Entity<IdentityRole>().ToTable("Role");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
        }

        #region Additional Tables
        //DbSet<Approver> Approver { get; set; }
        //DbSet<Department> Department { get; set; }
        //DbSet<UserDepartment> UserDepartment {get;set;}

        public DbSet<Approver> Approvers { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<UserDepartment> UserDepartments { get; set; }
        public DbSet<UserRoleRight> UserRoleRights { get; set; }
        #endregion

        public ApplicationDbContext()
            : base("rmsDB", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    [Table("Approver")]
    public class Approver
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("UserInfo")]
        public string UserId { get; set; }
        public string Type { get; set; }
        public byte isDependent { get; set; }

        public virtual ApplicationUser UserInfo { get; set; }
    }

    [Table("Department")]
    public class Department
    {
        [Key]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index]
        public string Code { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public string Creator { get; set; }
        public string DateCreated { get; set; }
        public string Modifier { get; set; }
        public string DateModified { get; set; }
        public bool IsActive { get; set; }
        //public ICollection<UserDepartment> Users { get; set; }
    }

    [Table("UserDepartment")]
    public class UserDepartment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [ForeignKey("DepartmentInfo")]
        public string DepartmentCode { get; set; }

        [ForeignKey("UserInfo")]
        [Index(IsUnique = true)]
        public string UserId { get; set; }

        public virtual ApplicationUser UserInfo { get; set; }
        public virtual Department DepartmentInfo { get; set; }
    }

    [Table("UserRoleRights")]
    public class UserRoleRight
    {
        [Key]
        [ForeignKey("UserInfo")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(128)]
        public string UserId { get; set; }


        [ForeignKey("RoleInfo")]
        [Column(TypeName = "NVARCHAR")]
        [StringLength(128)]
        [Index]
        public string RoleId { get; set; }

        public bool canCreate { get; set; }
        public bool canModify { get; set; }
        public bool viewOnly { get; set; }


        public virtual ApplicationUser UserInfo { get; set; }
        public virtual ApplicationRoles RoleInfo { get; set; }
    }
    
}