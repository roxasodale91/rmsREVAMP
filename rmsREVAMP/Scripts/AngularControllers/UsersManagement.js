﻿(function () {
    var RolesListModule = angular.module("AngularApp");

    RolesListModule.controller("UserController", function ($scope, $http, $filter, NgTableParams, UserService) { //Angular Controller

        $scope.users;
        $scope.userrole;
        $scope.register = {};

        get();

        function get() {
            UserService.get()
                .then(function (response) {
                    $scope.usersTable = new NgTableParams({
                        page: 1,
                        count: 5
                    },
                        {
                            total: response.data.length,
                            counts: [5, 10, 15, 20], //DISPLAY DATA ROW LENGTH
                            getData: function (params) {
                                params.total(response.data.length);
                                $scope.users = params.filter() ? $filter('filter')(response.data, params.filter()) : $scope.data;
                                $scope.users = response.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                return response.data;
                            }
                        }
                    );
                }, function (error) {
                    UIkit.notification({ message: 'Unable to Load Data', status: 'danger' });
                });
        }

        $scope.delete = function (id) {
            UserService.delete(id)
                .then(function (response) {
                    get();
                    UIkit.notification({ message: 'Role Deleted Successfully', status: 'success' });

                }, function (error) {
                    $scope.status = 'Unable to delete customer: ' + error.message;
                    UIkit.notification({ message: 'Role Delete Failed', status: 'danger' });
                });
        }

        debugger
        $scope.create = function () {
            UserService.create($scope.register)
                .then(function (response) {
                    console.log('Success');
                   //get();
                    UIkit.notification({ message: 'User Created Successfully', status: 'success' });
                }, function (error) {
                    UIkit.notification({ message: 'User Create Failed', status: 'danger' });
                    console.log('Failed');
                });
        };
    });

    RolesListModule.service('UserService', ['$http', function ($http) {

        var urlBase = '/UserManagement/';

        this.get = function () {
            return $http.get(urlBase + "/GetUsersListJson");
        }

        this.delete = function (id) {
            return $http.post(urlBase + 'DeleteRole/' + id);
        }

        this.create = function (register) {
            return $http.post('/Account/' + 'Register', register);
        }
    }]);

})();