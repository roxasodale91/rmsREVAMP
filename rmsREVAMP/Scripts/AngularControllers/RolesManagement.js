﻿(function () {

    var RolesListModule = angular.module("AngularApp", ['ngTable', 'ui.bootstrap', 'ngMessages']);

    RolesListModule.controller("AngularController", function ($scope, $http, $filter, NgTableParams, RoleService) { //Angular Controller

        $scope.roles;
        $scope.newrole;
        $scope.roledetails;
        $scope.saveDetails = {};
        get();

        function get() {
            RoleService.get()
                .then(function (response) {
                    $scope.rolesTable = new NgTableParams({
                        page: 1,
                        count: 5
                    },
                        {
                            total: response.data.length,
                            counts: [5, 10, 15, 20], //DISPLAY DATA ROW LENGTH
                            getData: function (params) {
                                params.total(response.data.length);
                                $scope.roles = params.filter() ? $filter('filter')(response.data, params.filter()) : $scope.data;
                                $scope.roles = response.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                return response.data;
                            }
                        }
                    );
                }, function (error) {
                    $scope.status = 'Unable to load customer data: ' + error.message;
                });
        }

        $('.checks').change(function () {
            $scope.newrole;
            var isActive = $('#validateactive');
            var materialCreate = $('#validatecreate');
            var materialView = $('#validateview');
            var materialModify = $('#validatemodify');

            var recipeView = $('#validateviewrec');
            var recipeCreate = $('#validatecreaterec');
            var recipeModify = $('#validatemodifyrec');

            var materialModCreate = $('#modvalidatecreate');
            var materialModView = $('#modvalidateview');
            var materialModModify = $('#modvalidatemodify');

            var recipeModView = $('#modvalidateviewrec');
            var recipeModCreate = $('#modvalidatecreaterec');
            var recipeModModify = $('#modvalidatemodifyrec');

            var isActiveModify = $('#modvalidatestatus');

            switch (this.id) {
                case "IsCanView":
                    console.log("IsCanView");
                    isActive.val($(this).prop('checked'));
                    break;

                case "matIsCanCreate":
                    console.log("matIsCanCreate");

                    materialCreate.val($(this).prop('checked'));
                    break;

                case "recIsCanCreate":
                    console.log("recIsCanCreate");

                    recipeCreate.val($(this).prop('checked'));
                    break;

                case "matIsCanModify":
                    console.log("matIsCanModify");
                    materialModify.val($(this).prop('checked'));
                    break;

                case "recIsCanModify":
                    console.log("recIsCanModify");
                    recipeModify.val($(this).prop('checked'));
                    break;

                case "matIsCanView":
                    console.log("matIsCanView");
                    materialView.val($(this).prop('checked'));
                    break;

                case "recIsCanView":
                    console.log("recIsCanView");
                    recipeView.val($(this).prop('checked'));
                    break;
                ////////////////////////////////////////////////
                case "matModIsCanCreate":
                    console.log("matModIsCanCreate");
                    materialModCreate.val($(this).prop('checked'));
                    break;

                case "matModIsCanModify":
                    console.log("matModIsCanCreate");
                    materialModModify.val($(this).prop('checked'));
                    break;

                case "matModIsCanView":
                    console.log("matModIsCanCreate");
                    materialModView.val($(this).prop('checked'));
                    break;
                ////////////////////////////////////////////////
                case "recModIsCanCreate":
                    console.log("recModIsCanCreate");
                    recipeModCreate.val($(this).prop('checked'));
                    break;

                case "recModIsCanModify":
                    console.log("recModIsCanModify");
                    recipeModModify.val($(this).prop('checked'));
                    break;

                case "recModIsCanView":
                    console.log("recModIsCanView");
                    recipeModView.val($(this).prop('checked'));
                    break;
                ////////////////////////////////////////////////
                case "statusMod":
                    console.log("statusMod");
                    isActiveModify.val($(this).prop('checked'));
                    break;
            }

            // Material
            if ((materialView.val() == "true")) {
                if (materialCreate.val() == "true") {
                    $('#matIsCanCreate').bootstrapToggle('off');
                    validatecreate.value = "false";

                    if (materialModify.val() == "true") {
                        $('#matIsCanModify').bootstrapToggle('off');
                        validatemodify.value = "false";

                    }
                }
                else if (materialModify.val() == "true") {
                    $('#matIsCanModify').bootstrapToggle('off');
                    validatemodify.value = "false";

                }
            }
            // RECIPE
            if ((recipeView.val() == "true")) {
                if (recipeCreate.val() == "true") {
                    $('#recIsCanCreate').bootstrapToggle('off');
                    recipeCreate.val("false");

                    if (recipeModify.val() == "true") {
                        $('#recIsCanModify').bootstrapToggle('off');
                        recipeModify.val("false");
                    }
                }
                else if (recipeModify.val() == "true") {
                    $('#recIsCanModify').bootstrapToggle('off');
                    recipeModify.val("false");
                }
            }


            // Role Details Modify
            // Material
            if ((materialModView.val() == "true")) {
                if (materialModCreate.val() == "true") {
                    $('#matModIsCanCreate').bootstrapToggle('off');
                    materialModCreate.val("false");

                    if (materialModModify.val() == "true") {
                        $('#matModIsCanModify').bootstrapToggle('off');
                        materialModModify.val("false");
                    }
                }
                else if (materialModModify.val() == "true") {
                    $('#matModIsCanModify').bootstrapToggle('off');
                    materialModModify.val("false");
                }
            }
            // Role Details Modify
            // Recipe
            if ((recipeModView.val() == "true")) {
                if (recipeModCreate.val() == "true") {
                    $('#recModIsCanCreate').bootstrapToggle('off');
                    recipeModCreate.val("false");

                    if (recipeModModify.val() == "true") {
                        $('#recModIsCanModify').bootstrapToggle('off');
                        recipeModModify.val("false");
                    }
                }
                else if (recipeModModify.val() == "true") {
                    $('#recModIsCanModify').bootstrapToggle('off');
                    recipeModModify.val("false");
                }
            }
        })

        $scope.getroledetailjson = function (newid) {
            RoleService.getroledetailjson(newid)
                .then(function (response) {
                    UIkit.notification({ message: 'Role Details Loaded', status: 'success' });
                    $scope.roledetails = response.data;

                    //////////////////STATUS///////////////////
                    if ($scope.roledetails.isActive == true) {
                        $('#statusMod').bootstrapToggle('on');
                    }
                    else
                        $('#statusMod').bootstrapToggle('off');
                    //////////////////MATERIAL///////////////////

                    if ($scope.roledetails.canCreateMaterial == true)
                    {
                        $('#matModIsCanCreate').bootstrapToggle('on');
                    }
                    else
                        $('#matModIsCanCreate').bootstrapToggle('off');

                    if ($scope.roledetails.canModifyMaterial == true) {
                        $('#matModIsCanModify').bootstrapToggle('on');
                    }
                    else
                        $('#matModIsCanModify').bootstrapToggle('off');

                    if ($scope.roledetails.isViewOnlyMaterial == true) {
                        $('#matModIsCanView').bootstrapToggle('on');
                    }
                    else
                        $('#matModIsCanView').bootstrapToggle('off');

                   //////////////////RECIPE///////////////////

                    if ($scope.roledetails.canCreateRecipe == true) {
                        $('#recModIsCanCreate').bootstrapToggle('on');
                    }
                    else
                        $('#recModIsCanCreate').bootstrapToggle('off');

                    if ($scope.roledetails.canModifyRecipe == true) {
                        $('#recModIsCanModify').bootstrapToggle('on');
                    }
                    else
                        $('#recModIsCanModify').bootstrapToggle('off');

                    if ($scope.roledetails.isViewOnlyRecipe == true) {
                        $('#recModIsCanView').bootstrapToggle('on');
                    }
                    else
                        $('#recModIsCanView').bootstrapToggle('off');
                }, function (error) {
                    UIkit.notification({ message: 'Role Details not Retrieved', status: 'danger' });
                });
        };

        $scope.delete = function (id) {
            RoleService.delete(id)
                .then(function (response) {
                    get();
                    UIkit.notification({ message: 'Role Deleted Successfully', status: 'success' });

                }, function (error) {
                    UIkit.notification({ message: 'Role Delete Failed', status: 'danger' });
                });
        }

        $scope.create = function () {
            $scope.newrole.canCreateMaterial = angular.element('#validatecreate').val();
            $scope.newrole.canmodifyMaterial = angular.element('#validatemodify').val();
            $scope.newrole.canViewMaterial = angular.element('#validateview').val();
            $scope.newrole.canCreateRecipe = angular.element('#validatecreaterec').val();
            $scope.newrole.canModifyRecipe = angular.element('#validatemodifyrec').val();
            $scope.newrole.canViewRecipe = angular.element('#validateviewrec').val();
            $scope.newrole.isActive = angular.element('#validateactive').val();

            RoleService.create($scope.newrole)
                .then(function (response) {
                    get();
                    $scope.newrole.Name = '';
                    $scope.newrole.Description = '';
                    UIkit.modal('#modal-createrole').hide();
                    UIkit.notification({ message: 'Role Created Successfully', status: 'success' });
                }, function (error) {
                    $scope.newrole.Name = '';
                    $scope.newrole.Description = '';
                    UIkit.modal('#modal-createrole').hide();
                    UIkit.notification({ message: 'Role Create Failed', status: 'danger' });

                });
        };

        debugger
        $scope.edit = function () {
            $scope.saveDetails.Name = $scope.roledetails.Name;
            $scope.saveDetails.Id = $scope.roledetails.Id;
            $scope.saveDetails.DateCreated = $scope.roledetails.DateCreated;
            $scope.saveDetails.Creator = $scope.roledetails.Creator;
            $scope.saveDetails.Description = $scope.roledetails.Description;
            $scope.saveDetails.canCreateMaterial = angular.element('#modvalidatecreate').val().toString();
            $scope.saveDetails.canmodifyMaterial =  angular.element('#modvalidatemodify').val().toString();
            $scope.saveDetails.isViewOnlyMaterial =  angular.element('#modvalidateview').val().toString();
            $scope.saveDetails.canCreateRecipe =  angular.element('#modvalidatecreaterec').val().toString();
            $scope.saveDetails.canModifyRecipe =  angular.element('#modvalidatemodifyrec').val().toString();
            $scope.saveDetails.isViewOnlyRecipe =  angular.element('#modvalidateviewrec').val().toString();
            $scope.saveDetails.isActive = angular.element('#modvalidatestatus').val().toString();
            

            RoleService.edit($scope.saveDetails)
                .then(function (response) {
                    get();
                    UIkit.modal('#modal-center').hide();
                    UIkit.notification({ message: 'Role Updated Successfully', status: 'success' });
                }, function (error) {
                    UIkit.notification({ message: 'Role Update Failed', status: 'danger' });

                });
        };
    });

    RolesListModule.service('RoleService', ['$http', function ($http) {

        var urlBase = '/RoleManagement/';

        this.getroledetailjson = function (newid)
        {
            return $http.post(urlBase + 'GetRoleDetailJson/' + newid);
        }

        this.get = function () {
            return $http.get(urlBase + "/GetRolesListJson");
        }

        this.delete = function (id) {
            return $http.post(urlBase + 'DeleteRole/' + id);
        }

        this.create = function (newrole) {
            return $http.post(urlBase + 'CreateRole', newrole);
        }

        this.edit = function (saveDetails) {
            return $http.post(urlBase + 'UpdateRole', saveDetails);
        }
        
    }]);
})();
