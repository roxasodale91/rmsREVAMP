﻿(function () {

    var DepartmentListModule = angular.module("AngularApp");

    DepartmentListModule.controller("DepartmentController", function ($scope, $http, $filter, NgTableParams, DepartmentService) { //Angular Controller

        $scope.departments;
        $scope.newdepartment = {};
        $scope.editdepartment = {};

        get();

        function get() {
            DepartmentService.get()
                .then(function (response) {
                    $scope.departmentsTable = new NgTableParams({
                        page: 1,
                        count: 5
                    },
                        {
                            total: response.data.length,
                            counts: [5, 10, 15, 20], //DISPLAY DATA ROW LENGTH
                            getData: function (params) {
                                params.total(response.data.length);
                                $scope.departments = params.filter() ? $filter('filter')(response.data, params.filter()) : $scope.data;
                                $scope.departments = response.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                return response.data;
                            }
                        }
                    );
                }, function (error) {
                    $scope.status = 'Unable to load customer data: ' + error.message;
                });
        }

        $scope.delete = function (id) {
            DepartmentService.delete(id)

                .then(function (response) {

                    $scope.status = 'Deleted Customer! Refreshing customer list.';
                    get();
                    UIkit.notification({ message: 'Department Deleted Successfully', status: 'success' });

                }, function (error) {
                    $scope.status = 'Unable to delete customer: ' + error.message;
                    UIkit.notification({ message: 'Department Delete Failed', status: 'danger' });
                });
        };


        $scope.create = function () {
            
            DepartmentService.create($scope.newdepartment)
                .then(function (response) {
                    get();
                    $scope.newdepartment.Name = '';
                    UIkit.notification({ message: 'Department Created Successfully', status: 'success' });
                }, function (error) {
                    UIkit.notification({ message: 'Department Create Failed', status: 'danger' });
                });
        };

        $scope.edit = function () {
            debugger
            DepartmentService.edit($scope.editdepartment)
                .then(function (response) {
                    get();
                    $scope.newdepartment.Name = '';
                    UIkit.notification({ message: 'Department Modified Successfully', status: 'success' });
                }, function (error) {
                    UIkit.notification({ message: 'Department Modify Failed', status: 'danger' });
                });
        };

        $scope.assignToTextBox = function (name,code) {
            UIkit.modal("#modal-center").show();
            $scope.editdepartment.Name = name;
            $scope.editdepartment.Code = code;
        };
    });

    DepartmentListModule.service('DepartmentService', ['$http', function ($http) {

        var urlBase = '/Department/';

        this.get = function () {
            return $http.get(urlBase + "/GetDepartmentsListJson");
        };

        this.delete = function (id) {
            return $http.post(urlBase + 'DeleteDepartment/' + id);
        };

        this.create = function (newrole) {
            return $http.post(urlBase + 'CreateDepartment', newrole);
        };

        this.edit = function (editdepartment) {
            return $http.post(urlBase + 'UpdateDepartment', editdepartment);
        };
    }]);


})();
