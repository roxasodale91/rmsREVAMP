﻿(function () {

    var SingleUserModule = angular.module("AngularApp");

    SingleUserModule.controller("ViewUserDetailsController", function ($scope,SingleUserService) { //Angular Controller
        
        $scope.roles = {};
        $scope.departments = {};
        $scope.newrole = {};
        $scope.newdepartment = {};
        $scope.getrolesofuser = {};
        $scope.getdepartmentsofuser = {};
        $scope.craterole = {};
        $scope.getRights = {};

        $('.checks').change(function ()
        {
            $scope.newrole;
            var isActive = $('#validateactive');
            var materialCreate = $('#validatecreate');
            var materialView = $('#validateview');
            var materialModify = $('#validatemodify');

            var recipeView = $('#validateviewrec');
            var recipeCreate = $('#validatecreaterec');
            var recipeModify = $('#validatemodifyrec');

            switch (this.id) {
                case "IsCanView":
                    console.log("IsCanView");
                    isActive.val($(this).prop('checked'));
                    break;

                case "matIsCanCreate":
                    console.log("matIsCanCreate");
                    //$('#validatecreate').val($(this).prop('checked'));

                    materialCreate.val($(this).prop('checked'));
                    break;

                case "recIsCanCreate":
                    console.log("recIsCanCreate");
                    //$('#validatecreaterec').val($(this).prop('checked'));
                    recipeCreate.val($(this).prop('checked'));
                    break;

                case "matIsCanModify":
                    console.log("matIsCanModify");
                    //$('#validatemodify').val($(this).prop('checked'));
                    materialModify.val($(this).prop('checked'));
                    break;

                case "recIsCanModify":
                    console.log("recIsCanModify");
                    //$('#validatemodifyrec').val($(this).prop('checked'));
                    recipeModify.val($(this).prop('checked'));
                    break;

                case "matIsCanView":
                    console.log("matIsCanView");
                    //$('#validateview').val($(this).prop('checked'));
                    materialView.val($(this).prop('checked'));
                    break;

                case "recIsCanView":
                    console.log("recIsCanView");
                    recipeView.val($(this).prop('checked'));
                    break;
            }
            // Material
            if ((materialView.val() == "true")) {
                if (materialCreate.val() == "true") {
                    $('#matIsCanCreate').bootstrapToggle('off');
                    validatecreate.value = "false";

                    if (materialModify.val() == "true") {
                        $('#matIsCanModify').bootstrapToggle('off');
                        validatemodify.value = "false";
                    }
                }
                else if (materialModify.val() == "true") {
                    $('#matIsCanModify').bootstrapToggle('off');
                    validatemodify.value = "false";
                }
            }
            // RECIPE
            if ((recipeView.val() == "true")) {
                if (recipeCreate.val() == "true") {
                    $('#recIsCanCreate').bootstrapToggle('off');
                    recipeCreate.val("false");

                    if (recipeModify.val() == "true") {
                        $('#recIsCanModify').bootstrapToggle('off');
                        recipeModify.val("false");
                    }
                }
                else if (recipeModify.val() == "true") {
                    $('#recIsCanModify').bootstrapToggle('off');
                    recipeModify.val("false");
                }
            }
        })

        $scope.test = function () {
            var roleID = ('IsCanView').toString();
            isActive.val($(this).prop('checked'));

            var what = angular.element(roleID).val();

            what.addClass('checked');
        };

        $scope.consolelog = function (role, event) {

            var ID = $(event.target).attr("id")
            //console.log(role + ID);

            var materialCreate = $('#validatecreate');
            var materialView = $('#validateview');
            var materialModify = $('#validatemodify');

            switch (ID) {
                case "CanCreate":
                    console.log("CanCreate");
                    materialCreate.val($(this).prop('checked'));
                    if ($scope.craterole.Create == true)
                    {
                        $scope.craterole.Create = false;
                    }
                    else
                        $scope.craterole.Create = true;
                    
                    break;

                case "CanModify":
                    console.log("CanModify");
                    materialModify.val($(this).prop('checked'));
                    break;

                case "ViewOnly":
                    console.log("ViewOnly");
                    materialView.val($(this).prop('checked'));
                    break;
            }

        };

        $scope.getRightsPerModule = function (ID) {

            $scope.ID = {};

            //Initialize Variables for Rights
            var roleID = ('#' + ID.toString() + 'name').toString();
            var getRightCreate = ('#' + ID.toString() + 'create').toString();
            var getRightModify = ('#' + ID.toString() + 'modify').toString();
            var getRightView = ('#' + ID.toString() + 'view').toString();

            //Retrieve Hidden Field Values
            $scope.ID.RoleId = angular.element(roleID).val();
            $scope.ID.canCreate = angular.element(getRightCreate).val();
            $scope.ID.canModify = angular.element(getRightModify).val();
            $scope.ID.viewOnly = angular.element(getRightView).val();

            if ($scope.ID.canCreate.length <= 1) {
                $scope.ID.canCreate = false;
            }
            if ($scope.ID.canModify.length <= 1) {
                $scope.ID.canModify = false;
            }
            if ($scope.ID.viewOnly.length <= 1) {
                $scope.ID.viewOnly = false;
            }

            console.log($scope.ID.RoleId);
            console.log($scope.ID.canCreate);
            console.log($scope.ID.canModify);
            console.log($scope.ID.viewOnly);
        };

        get();

        function get()
        {

            SingleUserService.getAvailableRoles()
                .then(function (response) {
                    $scope.status = 'Roles Loaded';
                    $scope.roles = response.data;
                },
                function (error) {
                    $scope.status = 'Unable to load Roles: ' + error.message;
                });

            SingleUserService.getAvailableDepartments()
                .then(function (response) {
                    $scope.status = 'Departments Loaded';
                    $scope.departments = response.data;
                },
                function (error) {
                    $scope.status = 'Unable to load Departmetns: ' + error.message;
                });

            SingleUserService.getRolesOfUser(angular.element('#UserId').val())
                .then(function (response) {
                    $scope.status = 'Roles Loaded';
                    $scope.rolesofuser = response.data;
                },
                function (error) {

                });

            SingleUserService.getDepartmentsOfUser(angular.element('#UserId').val())
                .then(function (response) {
                    $scope.status = 'Departments Loaded';
                    $scope.getdepartmentsofuser = response.data;
                },
                function (error) {

                });
        };

        $scope.insertUserRole = function () {

            $scope.newrole;
            $scope.newrole.UserId = angular.element('#UserId').val();

            SingleUserService.insertUserRole($scope.newrole)
                .then(function (response) {
                    console.log("Success");
                    UIkit.notification({ message: 'Role Assigned Successfully', status: 'success' });
                    get();
                },
                function (error) {
                    console.log("Error");
                    UIkit.notification({ message: 'Role Assignment Failed', status: 'danger' });
                });
        };

        $scope.insertUserDepartment = function () {
            $scope.newdepartment;
            $scope.newdepartment.UserId = angular.element('#UserId').val();

            SingleUserService.insertUserDepartment($scope.newdepartment)
                .then(function (response) {
                    console.log("Success");
                    UIkit.notification({ message: 'Department Assigned Successfully', status: 'success' });
                    get();
                },
                function (error) {
                    console.log("Error");
                    UIkit.notification({ message: 'Department Assignment Failed', status: 'danger' });
                });
        };

        $scope.deleteRoleOfUser = function (name)
        {
            $scope.getrolesofuser.RoleName = name;
            $scope.getrolesofuser.UserId = angular.element('#UserId').val();
            

            SingleUserService.deleteRoleOfUser($scope.getrolesofuser)
                .then(function (response) {
                    console.log("Success");
                    UIkit.notification({ message: 'Role Removed Successfully', status: 'success' });
                    get();
                },
                function (error) {
                    console.log("Error");
                    UIkit.notification({ message: 'Role Assignment Failed', status: 'danger' });
                });
        };

        $scope.deleteDepartmentOfUser = function (code) {
            $scope.getdepartmentsofuser = {};
            $scope.getdepartmentsofuser.DepartmentCode = code;
            $scope.getdepartmentsofuser.UserId = angular.element('#UserId').val();

            SingleUserService.deleteDepartmentOfUser($scope.getdepartmentsofuser)
                .then(function (response) {
                    console.log("Success");
                    UIkit.notification({ message: 'Role Removed Successfully', status: 'success' });
                    get();
                },
                function (error) {
                    console.log("Error");
                    UIkit.notification({ message: 'Role Assignment Failed', status: 'danger' });
                });
        };
    });

    SingleUserModule.service('SingleUserService', ['$http', function ($http) {

        var urlBase = '/UserManagement/';

        this.getAvailableRoles = function () {
            return $http.get('/RoleManagement/' + "GetRolesListJson");
        }

        this.getAvailableDepartments = function () {
            return $http.get('/Department/' + "GetDepartmentsListJson");
        }

        this.insertUserRole = function (newrole)
        {
            return $http.post(urlBase + "AddRoleToUser", newrole);
        }

        this.insertUserDepartment = function (newdepartment) {
            return $http.post(urlBase + "AddDepartmentToUser", newdepartment);
        }

        this.getRolesOfUser = function (id) {
            return $http.get(urlBase + "ViewRolesForUser/" + id);
        }

        this.getDepartmentsOfUser = function (id) {
            return $http.get(urlBase + "ViewDepartmentsForUser/" + id);
        }

        this.deleteRoleOfUser = function (getrolesofuser)
        {
            return $http.post(urlBase + "DeleteRoleFromUser", getrolesofuser);
        }

        this.deleteDepartmentOfUser = function (getdepartmentsofuser) {
            return $http.post(urlBase + "DeleteDepartmentToUser", getdepartmentsofuser);
        }
    }]);
})();