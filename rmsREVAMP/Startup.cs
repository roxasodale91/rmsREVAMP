﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(rmsREVAMP.Startup))]
namespace rmsREVAMP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
