﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using rmsREVAMP.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace rmsREVAMP.Controllers
{
    public class DepartmentController : Controller
    {
        ApplicationDbContext dbcontext = new ApplicationDbContext();
        RoleManagementViewModels rmvm = new RoleManagementViewModels();
        public class DepartmentDesignator
        {
            public string UserId { get; set; }
            public string DepartmentName { get; set; }
        }
        // GET: Department
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetDepartmentsListJson()
        {
            var data = dbcontext.Departments.OrderBy(a => a.Name).ToList();

            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetUsersListJson()
        {
            var data = dbcontext.Users.OrderBy(a => a.Email).ToList();

            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult ViewDepartmentOfUser( string id)
        {
            var data = dbcontext.UserDepartments.Where(a => a.UserId == id).Select(n => n.DepartmentCode).ToList();

            foreach (var item in data)
            {
                var departmentName = dbcontext.Departments.Where(b => b.Code == item).Single();
                rmvm.departmentList.Add(departmentName);
            }

            return new JsonResult { Data = rmvm.departmentList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult DeleteDepartment(string id)
        {

            var DelDepartmentById = dbcontext.Departments.Where(n => n.Code == id).ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                //dbcontext.UserDepartments.Remove(DelDepartmentById).Where(n => n.DepartmentCode == id);
                dbcontext.Departments.Remove(DelDepartmentById);
                dbcontext.SaveChanges();
            }

            return PartialView("PartialDepartmentList");
        }

        [HttpPost]
        public ActionResult CreateDepartment(Department department)
        {
            IdentityRole generateID = new IdentityRole();

            rmvm.department = department;
            rmvm.department.Code = generateID.Id;

            dbcontext.Departments.Add(rmvm.department);
            dbcontext.SaveChanges();

            ViewBag.ResultMessage = "New department created";

            return PartialView("PartialDepartmentList");
        }

        [HttpPost]
        public ActionResult UpdateDepartment(Department department)
        {
            Department departmentValidate = new Department();

            departmentValidate = dbcontext.Departments.Where(d=>d.Code == department.Code).FirstOrDefault<Department>();
            //departmentValidate.Code = department.Code;

            if (departmentValidate != null)
            {
                departmentValidate.Name = department.Name;
            }
            //departmentValidate.Name = department.Name;

            dbcontext.Entry(departmentValidate).State = System.Data.Entity.EntityState.Modified;
            //dbcontext.Departments.Add(rmvm.department);
            dbcontext.SaveChanges();

            //ViewBag.ResultMessage = "New department created";

            return PartialView("PartialDepartmentList");
        }
    }
}