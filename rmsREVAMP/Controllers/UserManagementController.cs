﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using rmsREVAMP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;

namespace rmsREVAMP.Controllers
{
    public class UserManagementController : Controller
    {
        ApplicationDbContext dbcontext = new ApplicationDbContext();
        RoleManagementViewModels rmvm = new RoleManagementViewModels();

        public JsonResult GetUsersListJson()
        {
            var data = dbcontext.Users.OrderBy(a => a.Email).ToList();

            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult ViewRolesForUser(string id)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbcontext));

            var RolesInList = UserManager.GetRoles(id).ToList();

            return new JsonResult { Data = RolesInList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult ViewDepartmentsForUser(string id)
        {
            var UserManager = dbcontext.UserDepartments.Where(n => n.UserId == id).Select(d => d.DepartmentCode).ToList();

            foreach(var item in UserManager)
            {
                var getDepartmentName = dbcontext.Departments.Where(n => n.Code == item).Single();
                rmvm.departmentList.Add(getDepartmentName);
            }
            //var RolesInList = UserManager.GetRoles(id).ToList();

            return new JsonResult { Data = rmvm.departmentList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        // GET: UserManagement
        public ActionResult Index()
        {
            rmvm.identityRoleList = dbcontext.Roles.ToList();
            return View(rmvm);
        }
        public ActionResult ViewUserInformation(string id)
        {
            rmvm.applicationUser = dbcontext.Users.SingleOrDefault(u => u.Id == id);
            rmvm.identityRoleList = dbcontext.Roles.ToList();

            return View(rmvm);
        }
        public ActionResult AddRoleToUser(RoleDesignator applicationUserRole)
        {
            try
            {
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbcontext));
                UserManager.AddToRole(
                        applicationUserRole.UserId,
                        applicationUserRole.RoleName);
                dbcontext.SaveChanges();

                return View("Index");
            }
            catch
            {
                return View("DO NOT ADD");
            }
            
        }
        public ActionResult AddDepartmentToUser(DepartmentDesignator departmentDesignator)
        {

            try {
                var CheckIfExist = dbcontext.UserDepartments
               .Where(n => n.DepartmentCode == departmentDesignator.DepartmentCode)
               .Where(n => n.UserId == departmentDesignator.UserId).Single();

                return View("DO NOT ADD");

            }
            catch
            {
                rmvm.userDepartment.UserId = departmentDesignator.UserId;
                rmvm.userDepartment.DepartmentCode = departmentDesignator.DepartmentCode;
                dbcontext.UserDepartments.Add(rmvm.userDepartment);
                dbcontext.SaveChanges();

                return View("Index");
            }
        }
        public ActionResult DeleteDepartmentToUser(DepartmentDesignator departmentDesignator)
        {

            var deleteDepartment = dbcontext.UserDepartments
                .Where(n => n.DepartmentCode == departmentDesignator.DepartmentCode)
                .Where(n => n.UserId == departmentDesignator.UserId).Single();

            dbcontext.UserDepartments.Remove(deleteDepartment);

            dbcontext.SaveChanges();

            return View("Index");
        }
        public ActionResult DeleteRoleFromUser(RoleDesignator applicationUserRole)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbcontext));
            UserManager.RemoveFromRole(
                    applicationUserRole.UserId,
                    applicationUserRole.RoleName);
            dbcontext.SaveChanges();

            return View("Index");
            //RedirectToAction("Index", controllerName: "ViewUserInformation", routeValues: applicationUserRole);
        }
        //public ActionResult AddRoleRightsToUser()
        //{

        //}
    }

    public class RoleDesignator
    {
        public string UserId { get; set; }
        public string RoleName { get; set; }
    }

    public class DepartmentDesignator
    {
        public string UserId { get; set; }
        public string DepartmentCode { get; set; }
    }
}