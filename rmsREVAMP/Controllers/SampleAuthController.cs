﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace rmsREVAMP.Controllers
{
    public class SampleAuthController : Controller
    {
        [Authorize(Roles = "CanCreateMaterials,SuperUser")]
        public ActionResult CreateMaterial()
        {
            return View();
        }

        [Authorize(Roles = "CanCreateRecipe,SuperUser")]
        public ActionResult CreateRecipe()
        {
            return View();
        }

        [Authorize(Roles = "SuperUser")]
        public ActionResult SuperUser()
        {
            return View();
        }
    }
}