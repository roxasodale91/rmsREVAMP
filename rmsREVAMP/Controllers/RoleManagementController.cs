﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using rmsREVAMP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace rmsREVAMP.Controllers
{
    [Authorize]
    public class RoleManagementController : Controller
    {
        ApplicationDbContext dbcontext = new ApplicationDbContext();
        RoleManagementViewModels rmvm = new RoleManagementViewModels();

        // GET: RoleManagement
        public ActionResult Index()
        {
            //GetRolesList();
            return View();
        }

        //Retrieve All Roles
        public RoleManagementViewModels GetRolesList()
        {
            //Set Rolemanager to Access Extended Identiy Role Class > ApplicationRoles
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbcontext));


            rmvm.identityRoleList = RoleManager.Roles.ToList();

            return rmvm;
        }

        [HttpPost]
        public ActionResult CreateRole(ApplicationRoles applicationRoles)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbcontext));
            ApplicationUser currentUser = UserManager.FindById(User.Identity.GetUserId());
            
            string Email = currentUser.Email;

            IdentityRole GetSystemGeneratedValue = new IdentityRole();
            
            applicationRoles.Id = GetSystemGeneratedValue.Id;
            applicationRoles.Creator = Email;
            applicationRoles.Modifier = Email;
            applicationRoles.DateCreated = DateTime.UtcNow.ToShortDateString();
            applicationRoles.DateModified = DateTime.UtcNow.ToShortDateString();
            rmvm.applicationRoles = applicationRoles;

            dbcontext.Roles.Add(applicationRoles);
            dbcontext.SaveChanges();

            ViewBag.ResultMessage = "New role created";
            GetRolesList();

            return PartialView("PartialRoleList");
        }

        [HttpPost]
        public ActionResult UpdateRole(ApplicationRoles applicationRoles)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbcontext));
            ApplicationUser currentUser = UserManager.FindById(User.Identity.GetUserId());

            string Email = currentUser.Email;


            IdentityRole roleValidate = dbcontext.Roles.AsNoTracking().Where(d => d.Id == applicationRoles.Id).Single<IdentityRole>();

            if (roleValidate != null)
            {
                applicationRoles.Modifier = Email;
                applicationRoles.DateModified = DateTime.UtcNow.ToShortDateString();

                dbcontext.Entry(applicationRoles).State = System.Data.Entity.EntityState.Modified;
                dbcontext.SaveChanges();
            }
            


            return PartialView("PartialRoleDetails");
        }

        public ActionResult DeleteRole(string id)
        {

            var DelRoleById = dbcontext.Roles.Where(n => n.Id == id).ToList().FirstOrDefault();

            if (ModelState.IsValid)
            {
                dbcontext.Roles.Remove(DelRoleById);
                dbcontext.SaveChanges();
            }

            return PartialView("PartialRoleList");
        }

        public JsonResult GetRolesListJson()
        {
            var data = dbcontext.Roles.OrderBy(a => a.Name).ToList();
            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetRoleDetailJson(string id)
        {
            var data = dbcontext
                .Roles
                .Where(a => a.Id == id)
                .Single();

            return new JsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        
    }
}