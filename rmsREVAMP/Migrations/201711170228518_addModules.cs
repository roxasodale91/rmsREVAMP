namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addModules : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Modules",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 100, unicode: false),
                        Name = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Code)
                .Index(t => t.Code);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Modules", new[] { "Code" });
            DropTable("dbo.Modules");
        }
    }
}
