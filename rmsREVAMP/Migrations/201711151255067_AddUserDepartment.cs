namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserDepartment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserDepartment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentCode = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.DepartmentCode, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.DepartmentCode)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDepartment", "UserId", "dbo.User");
            DropForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department");
            DropIndex("dbo.UserDepartment", new[] { "UserId" });
            DropIndex("dbo.UserDepartment", new[] { "DepartmentCode" });
            DropTable("dbo.UserDepartment");
        }
    }
}
