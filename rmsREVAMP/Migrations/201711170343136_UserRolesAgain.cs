namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserRolesAgain : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Rights", new[] { "Code" });
            CreateTable(
                "dbo.UserRoleRights",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(maxLength: 128),
                        canCreate = c.Boolean(nullable: false),
                        canModify = c.Boolean(nullable: false),
                        viewOnly = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Role", t => t.RoleId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            DropColumn("dbo.UserRole", "CanCreate");
            DropColumn("dbo.UserRole", "CanModify");
            DropColumn("dbo.UserRole", "ViewOnly");
            DropTable("dbo.Rights");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Rights",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 100, unicode: false),
                        Name = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Code);
            
            AddColumn("dbo.UserRole", "ViewOnly", c => c.Boolean());
            AddColumn("dbo.UserRole", "CanModify", c => c.Boolean());
            AddColumn("dbo.UserRole", "CanCreate", c => c.Boolean());
            DropForeignKey("dbo.UserRoleRights", "UserId", "dbo.User");
            DropForeignKey("dbo.UserRoleRights", "RoleId", "dbo.Role");
            DropIndex("dbo.UserRoleRights", new[] { "RoleId" });
            DropIndex("dbo.UserRoleRights", new[] { "UserId" });
            DropTable("dbo.UserRoleRights");
            CreateIndex("dbo.Rights", "Code");
        }
    }
}
