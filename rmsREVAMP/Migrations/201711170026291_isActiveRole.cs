namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isActiveRole : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Role", "isActive", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Role", "isActive");
        }
    }
}
