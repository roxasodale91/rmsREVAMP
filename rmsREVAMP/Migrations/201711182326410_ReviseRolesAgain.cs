namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReviseRolesAgain : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Role", "isViewOnlyMaterial", c => c.Boolean());
            AddColumn("dbo.Role", "canCreateMaterial", c => c.Boolean());
            AddColumn("dbo.Role", "canModifyMaterial", c => c.Boolean());
            AddColumn("dbo.Role", "isViewOnlyRecipe", c => c.Boolean());
            AddColumn("dbo.Role", "canCreateRecipe", c => c.Boolean());
            AddColumn("dbo.Role", "canModifyRecipe", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Role", "canModifyRecipe");
            DropColumn("dbo.Role", "canCreateRecipe");
            DropColumn("dbo.Role", "isViewOnlyRecipe");
            DropColumn("dbo.Role", "canModifyMaterial");
            DropColumn("dbo.Role", "canCreateMaterial");
            DropColumn("dbo.Role", "isViewOnlyMaterial");
        }
    }
}
