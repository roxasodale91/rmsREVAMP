namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uniqueDepartmentCode : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Department", "Code", c => c.String(maxLength: 50, unicode: false));
            CreateIndex("dbo.Department", "Code");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Department", new[] { "Code" });
            AlterColumn("dbo.Department", "Code", c => c.String());
        }
    }
}
