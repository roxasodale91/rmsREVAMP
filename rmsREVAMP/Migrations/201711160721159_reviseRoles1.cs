namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reviseRoles1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Role", "isViewOnlyMaterial", c => c.Boolean());
            AddColumn("dbo.Role", "canCreateMaterial", c => c.Boolean());
            AddColumn("dbo.Role", "canModifyMaterial", c => c.Boolean());
            AddColumn("dbo.Role", "isViewOnlyRecipe", c => c.Boolean());
            AddColumn("dbo.Role", "canCreateRecipe", c => c.Boolean());
            AddColumn("dbo.Role", "canModifyRecipe", c => c.Boolean());
            DropColumn("dbo.Role", "isViewOnly");
            DropColumn("dbo.Role", "canCreate");
            DropColumn("dbo.Role", "canModify");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Role", "canModify", c => c.Boolean());
            AddColumn("dbo.Role", "canCreate", c => c.Boolean());
            AddColumn("dbo.Role", "isViewOnly", c => c.Boolean());
            DropColumn("dbo.Role", "canModifyRecipe");
            DropColumn("dbo.Role", "canCreateRecipe");
            DropColumn("dbo.Role", "isViewOnlyRecipe");
            DropColumn("dbo.Role", "canModifyMaterial");
            DropColumn("dbo.Role", "canCreateMaterial");
            DropColumn("dbo.Role", "isViewOnlyMaterial");
        }
    }
}
