namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddinfotoUsers1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "Status", c => c.Boolean(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "Status", c => c.String());
        }
    }
}
