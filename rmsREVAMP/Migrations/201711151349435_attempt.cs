namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class attempt : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department");
            DropIndex("dbo.Department", new[] { "Code" });
            DropIndex("dbo.UserDepartment", new[] { "DepartmentCode" });
            DropPrimaryKey("dbo.Department");
            AlterColumn("dbo.Department", "Code", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.UserDepartment", "DepartmentCode", c => c.String(maxLength: 50, unicode: false));
            AddPrimaryKey("dbo.Department", "Code");
            CreateIndex("dbo.Department", "Code");
            CreateIndex("dbo.UserDepartment", "DepartmentCode");
            AddForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department", "Code");
            DropColumn("dbo.Department", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Department", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department");
            DropIndex("dbo.UserDepartment", new[] { "DepartmentCode" });
            DropIndex("dbo.Department", new[] { "Code" });
            DropPrimaryKey("dbo.Department");
            AlterColumn("dbo.UserDepartment", "DepartmentCode", c => c.Int(nullable: false));
            AlterColumn("dbo.Department", "Code", c => c.String(maxLength: 50, unicode: false));
            AddPrimaryKey("dbo.Department", "Id");
            CreateIndex("dbo.UserDepartment", "DepartmentCode");
            CreateIndex("dbo.Department", "Code");
            AddForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department", "Id", cascadeDelete: true);
        }
    }
}
