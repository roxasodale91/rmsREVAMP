namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDepartment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Department", "Creator", c => c.String());
            AddColumn("dbo.Department", "DateCreated", c => c.String());
            AddColumn("dbo.Department", "Modifier", c => c.String());
            AddColumn("dbo.Department", "DateModified", c => c.String());
            AddColumn("dbo.Department", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Department", "IsActive");
            DropColumn("dbo.Department", "DateModified");
            DropColumn("dbo.Department", "Modifier");
            DropColumn("dbo.Department", "DateCreated");
            DropColumn("dbo.Department", "Creator");
        }
    }
}
