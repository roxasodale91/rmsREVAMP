namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReviseApplicationUser : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.User", "isApprover");
            DropColumn("dbo.User", "DepartmentCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.User", "DepartmentCode", c => c.String());
            AddColumn("dbo.User", "isApprover", c => c.Byte(nullable: false));
        }
    }
}
