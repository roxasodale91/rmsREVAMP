namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsROles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Role", "Creator", c => c.String());
            AddColumn("dbo.Role", "DateCreated", c => c.String());
            AddColumn("dbo.Role", "Modifier", c => c.String());
            AddColumn("dbo.Role", "DateModified", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Role", "DateModified");
            DropColumn("dbo.Role", "Modifier");
            DropColumn("dbo.Role", "DateCreated");
            DropColumn("dbo.Role", "Creator");
        }
    }
}
