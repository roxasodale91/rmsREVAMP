// <auto-generated />
namespace rmsREVAMP.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addModules1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addModules1));
        
        string IMigrationMetadata.Id
        {
            get { return "201711170306445_addModules1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
