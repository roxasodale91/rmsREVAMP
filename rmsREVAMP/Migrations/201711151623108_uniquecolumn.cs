namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uniquecolumn : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Department", "Name", c => c.String(maxLength: 50, unicode: false));
            CreateIndex("dbo.Department", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Department", new[] { "Name" });
            AlterColumn("dbo.Department", "Name", c => c.String());
        }
    }
}
