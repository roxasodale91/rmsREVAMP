namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniqueUserIDonUserDepartments : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.UserDepartment", new[] { "UserId" });
            CreateIndex("dbo.UserDepartment", "UserId", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserDepartment", new[] { "UserId" });
            CreateIndex("dbo.UserDepartment", "UserId");
        }
    }
}
