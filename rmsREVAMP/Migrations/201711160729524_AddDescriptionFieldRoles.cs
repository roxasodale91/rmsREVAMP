namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDescriptionFieldRoles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Role", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Role", "Description");
        }
    }
}
