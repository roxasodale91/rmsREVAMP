namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addModules1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Modules", newName: "Rights");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Rights", newName: "Modules");
        }
    }
}
