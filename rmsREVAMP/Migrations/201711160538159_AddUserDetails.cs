namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "DateCreated", c => c.String());
            AddColumn("dbo.User", "DateModified", c => c.String());
            AddColumn("dbo.User", "Creator", c => c.String());
            AddColumn("dbo.User", "Modifier", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "Modifier");
            DropColumn("dbo.User", "Creator");
            DropColumn("dbo.User", "DateModified");
            DropColumn("dbo.User", "DateCreated");
        }
    }
}
