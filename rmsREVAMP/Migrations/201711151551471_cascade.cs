namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cascade : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department");
            DropIndex("dbo.UserDepartment", new[] { "DepartmentCode" });
            AlterColumn("dbo.UserDepartment", "DepartmentCode", c => c.String(nullable: false, maxLength: 50, unicode: false));
            CreateIndex("dbo.UserDepartment", "DepartmentCode");
            AddForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department", "Code", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department");
            DropIndex("dbo.UserDepartment", new[] { "DepartmentCode" });
            AlterColumn("dbo.UserDepartment", "DepartmentCode", c => c.String(maxLength: 50, unicode: false));
            CreateIndex("dbo.UserDepartment", "DepartmentCode");
            AddForeignKey("dbo.UserDepartment", "DepartmentCode", "dbo.Department", "Code");
        }
    }
}
