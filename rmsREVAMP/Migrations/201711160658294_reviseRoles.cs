namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reviseRoles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Role", "isViewOnly", c => c.Boolean());
            AddColumn("dbo.Role", "canCreate", c => c.Boolean());
            AddColumn("dbo.Role", "canModify", c => c.Boolean());
            DropColumn("dbo.Role", "isViewOnlyMaterial");
            DropColumn("dbo.Role", "canCreateMaterial");
            DropColumn("dbo.Role", "canModifyMaterial");
            DropColumn("dbo.Role", "isViewOnlyRecipe");
            DropColumn("dbo.Role", "canCreateRecipe");
            DropColumn("dbo.Role", "canModifyRecipe");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Role", "canModifyRecipe", c => c.Boolean());
            AddColumn("dbo.Role", "canCreateRecipe", c => c.Boolean());
            AddColumn("dbo.Role", "isViewOnlyRecipe", c => c.Boolean());
            AddColumn("dbo.Role", "canModifyMaterial", c => c.Boolean());
            AddColumn("dbo.Role", "canCreateMaterial", c => c.Boolean());
            AddColumn("dbo.Role", "isViewOnlyMaterial", c => c.Boolean());
            DropColumn("dbo.Role", "canModify");
            DropColumn("dbo.Role", "canCreate");
            DropColumn("dbo.Role", "isViewOnly");
        }
    }
}
