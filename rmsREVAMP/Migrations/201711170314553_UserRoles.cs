namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserRoles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserRole", "CanCreate", c => c.Boolean());
            AddColumn("dbo.UserRole", "CanModify", c => c.Boolean());
            AddColumn("dbo.UserRole", "ViewOnly", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserRole", "ViewOnly");
            DropColumn("dbo.UserRole", "CanModify");
            DropColumn("dbo.UserRole", "CanCreate");
        }
    }
}
