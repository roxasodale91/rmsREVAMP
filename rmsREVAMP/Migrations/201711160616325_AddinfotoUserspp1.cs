namespace rmsREVAMP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddinfotoUserspp1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "Status", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "Status", c => c.Boolean(nullable: false));
        }
    }
}
